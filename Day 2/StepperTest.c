// defines pins numbers
const int stepPin = 2; 
const int dirPin = 5; 
const int ENPin =  8;
 
void setup() {
  // Sets the two pins as Outputs
  pinMode(13, OUTPUT); // Led pin
  pinMode(stepPin,OUTPUT); 
  pinMode(dirPin,OUTPUT);
  pinMode(ENPin, OUTPUT);
  digitalWrite(ENPin, LOW);
  Serial.println("Ready Now"); // Ready to receive commands
  Serial.println("Please enter Steps"); 
}

void turn(int dir,int steps)
{
 int i;
  for(i=0;i<steps;i++)
  {
    digitalWrite(stepPin,1);
    delay(100);
    digitalWrite(stepPin,0);
    delay(100);
  }

}

void loop() {
  digitalWrite(dirPin,HIGH);
  for(int x = 0; x < 200; x++) {
    digitalWrite(stepPin,HIGH); 
    delay(1); 
    digitalWrite(stepPin,LOW); 
    delay(1); 
  }
  delay(1000); // One second delay
  
  digitalWrite(dirPin,LOW);
  for(int x = 0; x < 400; x++) {
    digitalWrite(stepPin,HIGH);
    delay(1);
    digitalWrite(stepPin,LOW);
    delay(1);
  }
  delay(1000);
}
