// defines pins numbers
const int stepPin = 2; 
const int dirPin = 5; 
const int ENPin =  8;
 
void setup() {
  // Sets the two pins as Outputs
  pinMode(13, OUTPUT); // Led pin
  pinMode(stepPin,OUTPUT); 
  pinMode(dirPin,OUTPUT);
  pinMode(ENPin, OUTPUT);
  digitalWrite(ENPin, LOW);
  Serial.println("Ready Now"); // Ready to receive commands
  Serial.println("Please enter Steps"); 
}

void turn(int dir,int steps)
{
  int i;
  for(i=0;i<steps;i++)
  {
    digitalWrite(stepPin,1);
    delay(100);
    digitalWrite(stepPin,0);
    delay(100);
  }

}

void loop() {
 if(Serial.available() > 0) { // A byte is ready to receive
  steps_serial = Serial.parseInt();
  if(steps_serial == 90) { // byte is 'a'
  turn(1,steps_serial);  
  Serial.println("Motor turned");
  delay(2000);
}

}
